package com.example.emailtest;

import com.example.emailtest.email.MalformedMailException;
import com.fasterxml.jackson.databind.JsonMappingException;
import lombok.AllArgsConstructor;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
@AllArgsConstructor
public class UserController {
    private final UserRepository userRepository;


    @GetMapping
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @PostMapping
    public User createUser(@RequestBody UserDto userDto) {
        return userRepository.save(new User(null, userDto.getEmail()));

    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public String handleParseException(HttpMessageNotReadableException ex) {
        if (ex.getCause() instanceof JsonMappingException) {
            JsonMappingException jme = (JsonMappingException) ex.getCause();

            if (jme.getCause() instanceof MalformedMailException) {
                return "field: \"" + jme.getPath().get(0).getFieldName() + "\" invalid " + jme.getCause().getMessage();
            }

        }
        return "parse error";
    }
}
