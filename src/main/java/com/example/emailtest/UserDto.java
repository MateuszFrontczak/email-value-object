package com.example.emailtest;

import com.example.emailtest.email.ValidEmail;
import lombok.Data;

@Data
public class UserDto {
    private ValidEmail email;
}
