package com.example.emailtest.email.serializers;

import com.example.emailtest.email.ValidEmail;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

public class EmailSerializer extends StdSerializer<ValidEmail> {
    protected EmailSerializer() {
        super(ValidEmail.class);
    }

    @Override
    public void serialize(ValidEmail value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeString(value.toString());
    }
}
