package com.example.emailtest.email.serializers;

import com.example.emailtest.email.ValidEmail;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

public class EmailDeserializer extends StdDeserializer<ValidEmail> {
    protected EmailDeserializer() {
        super(ValidEmail.class);
    }

    @Override
    public ValidEmail deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        return ValidEmail.fromString(p.getCodec().readValue(p, String.class));
    }
}
