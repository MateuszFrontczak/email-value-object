package com.example.emailtest.email.serializers;

import com.example.emailtest.email.ValidEmail;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class ValidEmailConverter implements
        AttributeConverter<ValidEmail, String> {


    @Override
    public String convertToDatabaseColumn(ValidEmail email) {
        if (email == null) {
            return null;
        }

        return email.toString();
    }

    @Override
    public ValidEmail convertToEntityAttribute(String dbEmail) {
        if (dbEmail == null || dbEmail.isEmpty()) {
            return null;
        }
        return ValidEmail.fromString(dbEmail);
    }
}