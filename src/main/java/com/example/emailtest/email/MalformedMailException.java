package com.example.emailtest.email;

public class MalformedMailException extends RuntimeException {
    public MalformedMailException() {
        super("Email not matches regex");
    }
}
