package com.example.emailtest.email;

import com.example.emailtest.email.serializers.EmailDeserializer;
import com.example.emailtest.email.serializers.EmailSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.util.regex.Pattern;

@JsonDeserialize(using = EmailDeserializer.class)
@JsonSerialize(using = EmailSerializer.class)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ValidEmail {
    private String value;

    private static Pattern VALID_EMAIL_PATTERN = Pattern.compile("[A-z.]+@[A-z]+(\\.[A-z]+)*");
    public static ValidEmail fromString(String value) {
        if(!VALID_EMAIL_PATTERN.matcher(value).matches()){
            throw new MalformedMailException();
        }
        return new ValidEmail(value.toLowerCase().trim());
    }

    @Override
    public String toString() {
        return value;
    }
}
